//
//  ViewController.h
//  BackgroundTest
//
//  Created by 佐々木 涼介 on 2013/03/08.
//  Copyright (c) 2013年 ryosuke sasaki. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIButton *buttonR;
@property (retain, nonatomic) IBOutlet UIButton *buttonG;
@property (retain, nonatomic) IBOutlet UIButton *buttonB;
- (IBAction)touchUpInside:(id)sender;

@end
