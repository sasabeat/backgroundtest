//
//  AppDelegate.h
//  BackgroundTest
//
//  Created by 佐々木 涼介 on 2013/03/08.
//  Copyright (c) 2013年 ryosuke sasaki. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
