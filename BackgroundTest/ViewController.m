//
//  ViewController.m
//  BackgroundTest
//
//  Created by 佐々木 涼介 on 2013/03/08.
//  Copyright (c) 2013年 ryosuke sasaki. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_buttonR release];
    [_buttonG release];
    [_buttonB release];
    [super dealloc];
}

- (IBAction)touchUpInside:(id)sender
{
    if([sender isEqual:self.buttonR])
    {
        self.view.backgroundColor = [UIColor redColor];
    }
    if([sender isEqual:self.buttonG])
    {
        self.view.backgroundColor = [UIColor greenColor];
    }
    if([sender isEqual:self.buttonB])
    {
        self.view.backgroundColor = [UIColor blueColor];
    }
}
@end
