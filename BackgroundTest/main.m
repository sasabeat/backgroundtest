//
//  main.m
//  BackgroundTest
//
//  Created by 佐々木 涼介 on 2013/03/08.
//  Copyright (c) 2013年 ryosuke sasaki. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
